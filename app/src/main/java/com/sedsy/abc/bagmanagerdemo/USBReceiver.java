package com.sedsy.abc.bagmanagerdemo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.widget.Toast;

public class USBReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch(action)
        {
            case UsbManager.ACTION_USB_DEVICE_ATTACHED:
                Toast.makeText(context, "attached", Toast.LENGTH_SHORT).show();
                break;
            case UsbManager.ACTION_USB_DEVICE_DETACHED:
                Toast.makeText(context, "deattached", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("warning");
                builder.setCancelable(false);
                builder.setMessage("USB设备被拔出，程序即将退出");
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCollector.finishAll();
                    }
                });
                AlertDialog dialog = builder.create();
//                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.show();
                break;
        }
    }
}
