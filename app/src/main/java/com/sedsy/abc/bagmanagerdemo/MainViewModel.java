package com.sedsy.abc.bagmanagerdemo;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.AsyncTask;
import android.view.View;

import com.sedsy.abc.baggetter.BagManager;
import com.sedsy.abc.baggetter.Result;

public class MainViewModel extends BaseObservable {
    BagManager bagManager;
    Context context;
    boolean deviceStatus;
    @Bindable
    public boolean getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(boolean deviceStatus) {
        this.deviceStatus = deviceStatus;
        notifyPropertyChanged(BR.deviceStatus);
    }

    public MainViewModel(Context context) {
        this.context = context;
        bagManager = BagManager.getInstance(context);
        bagSizeEnum = BagManager.BagSizeEnum.BigBag;
        smallBagStatus = "未知";
        bigBagStatus = "未知";
    }
    public void onOpenDeviceClick(View view){
        if(bagManager.isConnected()){
            bagManager.disconnect();
        }
        else{
            bagManager.connect();
        }
        setDeviceStatus(bagManager.isConnected());
    }

    public void onBigBagClick(View view){
        executeIn(BagManager.BagSizeEnum.BigBag);
    }
    public void onSmallBagClick(View view){
        executeIn(BagManager.BagSizeEnum.SmallBag);
    }
    DeviceDataReader reader ;
    String msg;
    String smallBagStatus;
    String bigBagStatus;
    @Bindable
    public String getSmallBagStatus() {
        return smallBagStatus;
    }

    public void setSmallBagStatus(String smallBagStatus) {
        this.smallBagStatus = smallBagStatus;
        notifyPropertyChanged(BR.smallBagStatus);
    }
    @Bindable
    public String getBigBagStatus() {
        return bigBagStatus;
    }

    public void setBigBagStatus(String bigBagStatus) {
        this.bigBagStatus = bigBagStatus;
        notifyPropertyChanged(BR.bigBagStatus);
    }

    @Bindable
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    BagManager.BagSizeEnum bagSizeEnum;

    public BagManager.BagSizeEnum getBagSizeEnum() {
        return bagSizeEnum;
    }

    public void setBagSizeEnum(BagManager.BagSizeEnum bagSizeEnum) {
        this.bagSizeEnum = bagSizeEnum;
    }

    void executeIn(BagManager.BagSizeEnum bagSizeEnum){
        setBagSizeEnum(bagSizeEnum);
//        if(reader == null ||reader.getStatus() == AsyncTask.Status.PENDING || reader.getStatus() == AsyncTask.Status.FINISHED)
//        {
//            reader = new DeviceDataReader(this);
//            reader.execute(bagManager);
//        }
//        else
//            reader.cancel(true);
        reader = new DeviceDataReader(this);
        reader.execute(bagManager);
    }

}
class DeviceDataReader extends AsyncTask<BagManager,Result,Result>{
    MainViewModel vm;

    public DeviceDataReader(MainViewModel vm) {
        this.vm = vm;
    }

    @Override
    protected void onProgressUpdate(Result... values) {
        super.onProgressUpdate(values);
        if(vm.getBagSizeEnum() == BagManager.BagSizeEnum.BigBag)
            vm.setBigBagStatus(values[0].getResultDescription());
        else
            vm.setSmallBagStatus(values[0].getResultDescription());
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(vm.getBagSizeEnum() == BagManager.BagSizeEnum.BigBag)
            vm.setBigBagStatus(result.getResultDescription());
        else
            vm.setSmallBagStatus(result.getResultDescription());
    }

    @Override
    protected Result doInBackground(BagManager... bagManagers) {
        BagManager manager = bagManagers[0];
        Result result = null;
        try {
            result = manager.spitBag(vm.getBagSizeEnum());
            onProgressUpdate(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

