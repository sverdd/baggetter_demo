package com.sedsy.abc.bagmanagerdemo;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

public class ActivityCollector {
    static List<Activity> list = new ArrayList<Activity>();
    public static void remove(Activity a){
        list.remove(a);
    }
    public static void add(Activity a){
        list.add(a);
    }
    public static void finishAll(){
        for (Activity a : list){
            if(!a.isFinishing()){
                a.finish();
            }
        }
        list.clear();
    }
}
