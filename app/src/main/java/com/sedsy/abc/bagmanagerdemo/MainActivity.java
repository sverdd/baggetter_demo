package com.sedsy.abc.bagmanagerdemo;

import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.databinding.library.baseAdapters.BR;
import com.sedsy.abc.baggetter.BagManager;
import com.sedsy.abc.baggetter.Result;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
//        setContentView(R.layout.activity_main);
        USBReceiver usbReceiver = new USBReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        this.registerReceiver(usbReceiver, filter);

        MainViewModel vm = new MainViewModel(this);
        binding.setVariable(BR.vm,vm);

//        final BagManager manager = BagManager.getInstance(MainActivity.this);
//        manager.connect();//打开设备
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Result result = null;
//                    result = manager.spitBag(BagManager.BagSizeEnum.BigBag);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
    }
}
